<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'swenergy_db');

/** MySQL database username */
define('DB_USER', 'swenergy_user');

/** MySQL database password */
define('DB_PASSWORD', '8B=#?m#@@2di');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sNKKtY_L54UVrCLCaDHkq9doonSoEyPDIQ7kFef0GtlXgQEJZMfuk7Tn7g0Zg9vB');
define('SECURE_AUTH_KEY',  'DQvt5dmwDhI40XsQxoaYrLptrGyl8A0zGkgS8ICXb2OJCe45jTblgJqTz7Y7GNvW');
define('LOGGED_IN_KEY',    '5lEnKIgy0sdQGpjO1Ckux2Oh4N0ZUpePbNptjsZ1UeUACdCVP16DwE_Jtom5PBS7');
define('NONCE_KEY',        'vDNNOzwobUGzBmv2_CcNrlwsi4NMLav3VpaspJw_c9d1JevfDNMQnTVWBFjQDk4j');
define('AUTH_SALT',        'k_bHaePRRiC1r6V23jETCP7a97s7xr66DqGtodFR347c9yqdt0AqpJNboI7tjIdt');
define('SECURE_AUTH_SALT', 'XV4doTT4gCEFHrqvgXuYOSLKxdxB3C5sEq01tba_Co4pXpl20OBaxkoxPVYivUHM');
define('LOGGED_IN_SALT',   'zYXMmp9ZA5nKpKzePNVJd9CvWmju6wvyCEWpET_K9yJDNKOREEwLqieGTwMRK7ZF');
define('NONCE_SALT',       'IBpOvVSGUvt7_VEmrPIyIAra6aWPNSBozmkUy3S9eaaMXgaJremGEPlbG2N3xfSe');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sw_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
